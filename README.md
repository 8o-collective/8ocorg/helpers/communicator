# @8ocorg/communicator

Enables a subdomain to communicate with other subdomains, specifically getting and setting localStorage.