const enableCommunication = () => {
  document.domain = window.domain.split(":")[0];

  const getSelection = window.getSelection;
  window.getSelection = () =>
    window.self === window.top ? getSelection() : { type: "None" };

  window.onmessage = (e) => {
    // if (!e.origin.endsWith(window.domain)) return;
    const payload = JSON.parse(e.data);
    switch (payload.method) {
      case "set":
        localStorage.setItem(payload.key, JSON.stringify(payload.data));
        break;
      case "get":
        window.parent.postMessage(localStorage.getItem(payload.key), "*");
        break;
      case "remove":
        localStorage.removeItem(payload.key);
        break;
    }
  };
};

class Communicator {
  constructor(subdomains, domain = window.domain) {
    this.subdomains = subdomains;
    this.domain = domain;

    document.communicatorElement = document.head.appendChild(
      document.createElement("a")
    ); // needed by SubdomainFrames

    this.frames = this.subdomains.map(
      (subdomain) => new SubdomainFrame(subdomain, domain)
    );
    this.frames.forEach((frame) => (this[frame.subdomain] = frame));
  }
}

class SubdomainFrame {
  constructor(subdomain, domain) {
    this.subdomain = subdomain;
    this.domain = domain;
    this.host = `${window.location.protocol}//${
      this.subdomain === "main" ? "" : this.subdomain + "."
    }${this.domain}`;

    const handleMaintenance = (w) => {
      // w.console.log = () => { /* nop */ };
      w.document.onvisibilitychange = () => {
        /* nop */
      }; // disable screenshot
    };

    this.iframe = document.createElement("iframe");
    this.frameWindow = new Promise((resolve, reject) => {
      this.iframe.onload = () => {
        const w = this.iframe.contentWindow;
        handleMaintenance(w);

        return resolve(w);
      };

      this.iframe.onerror = (e) => {
        return reject(e);
      };
    });

    this.iframe.src = this.host;
    this.iframe.style = `position: absolute; top: 0px; height: 0px; width: 0px; visibility: hidden;`;
    document.communicatorElement.appendChild(this.iframe); // i do not CARE about w3c anymore
  }

  get(key) {
    return this.frameWindow.then((w) => {
      w.postMessage(JSON.stringify({ key, method: "get" }), "*");

      return new Promise((resolve) => {
        window.onmessage = (e) => {
          if (!e.origin.startsWith(this.host)) return;

          return resolve(e.data);
        };
      });
    });
  }

  set(key, data) {
    return this.frameWindow.then((w) =>
      w.postMessage(JSON.stringify({ key, method: "set", data }), "*")
    );
  }
}

const getShard = (domain = window.domain) => {
  const discovered = localStorage.getItem("discoveredShard");

  if (discovered) {
    return Promise.resolve(JSON.parse(discovered));
  }

  let comm = new Communicator(["main"], domain);
  return comm.main.get("key").then((keyString) => {
    const key = JSON.parse(keyString);

    return comm.main
      .get("lastShardAccessed")
      .then((lastShardAccessedString) => {
        const lastShardAccessed = JSON.parse(lastShardAccessedString);
        const newShardAccessed = lastShardAccessed + 1;

        const shard = key[newShardAccessed];
        comm.main.set("lastShardAccessed", newShardAccessed);

        localStorage.setItem("discoveredShard", JSON.stringify(shard));

        return shard;
      });
  });
};

module.exports = {
  enableCommunication,
  Communicator,
  getShard,
};
